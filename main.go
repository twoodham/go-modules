package main

import (
	"bytes"
	"crypto"
	"crypto/md5"
	"fmt"
	"log"
	"strconv"
)

const tmpl = ""

func main() {
	// Logger configuration
	var (
		buf    bytes.Buffer
		logger = log.New(&buf, "logger: ", log.Lshortfile)
	)

	logger.Print("A walrus appears")
	fmt.Print(&buf)

	// Weak crypto
	m := md5.New()
	str := "test"
	m.Write([]byte(str))
	fmt.Printf("%x\n", m.Sum(nil))

	// Weak crypto
	crypto.RegisterHash(crypto.MD5, md5.New)
	fmt.Println(crypto.MD5.Available())
	cm := crypto.MD5.New()
	cm.Write([]byte(str))
	//fmt.Printf("%x\n", cm.Sum(nil))
	fmt.Printf("%x\n", cm.Sum([]byte("test")))

	// Hardcoded secrets
	const passwd = "really-poor-practice"
	fmt.Println("Let's hardcode a password: ", passwd)

	// Integer overflow
	// v, _ := strconv.ParseInt("-354634382", 10, 32)
	// fmt.Printf("%T, %v", v, v)
	value := 10
	if value == 10 {
		value, _ := strconv.Atoi("2147483648")
		fmt.Println(int32(value))
	}
	v := int32(value)
	fmt.Println(v)
}
